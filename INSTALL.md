INSTALLATION
============

test-cryo-con runs on Debian 10 or Ubuntu 20

Install prerequesites
----------------------
```
sudo apt install libmodbus-dev qt5-qmake qtbase5-dev-tools qtbase5-dev
```

To build and install
---------------------
```
/usr/lib/x86_64-linux-gnu/qt5/bin/qmake
make
```


To run
-------
```
./bin/test-cryo-con-ok IP_ADDRESS
```

or

```
./bin/test-cryo-con-nok IP_ADDRESS
```

For example if the Cryo-con 18i has the IP address 10.251.251.10

```
./bin/test-cryo-con-ok 10.251.251.10
```

# ###########################################################################
# Copyright (C) 2021 Institut de RadioAstronomie Millimetrique
#
# ABSTRACT: Project file for test-cryo-con
#
# AUTHOR: Sebastien BLANCHET
#
# CREATION DATE: Aug 11, 2021
# ###########################################################################

include( conf/starting.pri )

TEMPLATE = subdirs
CONFIG *= ordered
SUBDIRS = apps

include( conf/ending.pri )

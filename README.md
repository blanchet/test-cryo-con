# test-cryo-con

Several test programs for [Cryo-Con 18i](https://www.cryocon.com/M18IProdFolder.php)

| Program name        | Description                                    |
|---------------------|------------------------------------------------|
| `test-cryo-con-ok`  | Mono modbus connection, works normally         |
| `test-cryo-con-nok` | Multi modbus connection, crashes cryo-con      |


To build the program, [see instructions in INSTALL.md](INSTALL.md)

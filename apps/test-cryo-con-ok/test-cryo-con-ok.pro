# qmake file for test-cryo-con-ok
include(../../conf/starting.pri)

TEMPLATE = app
TARGET = test-cryo-con-ok


QT = core

QMAKE_CFLAGS   *= $$system( pkg-config --cflags libmodbus )
QMAKE_CXXFLAGS *= $$system( pkg-config --cflags libmodbus )
LIBS           *= $$system( pkg-config --libs   libmodbus )


#HEADERS *= $$system( ls *.hpp )

SOURCES *= $$system( ls *.cpp )

include(../../conf/ending.pri)
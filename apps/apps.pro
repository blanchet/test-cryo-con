# ###########################################################################
# Copyright (C) 2012 Institut de RadioAstronomie Millimetrique
#
# ABSTRACT: Project file for apps directory
#
# AUTHOR: Sebastien BLANCHET
#
# CREATION DATE: Sep 17, 2013
# ###########################################################################
include(../conf/starting.pri)
TEMPLATE = subdirs
SUBDIRS = \
        test-cryo-con-ok        \
        test-cryo-con-nok       \

include(../conf/ending.pri)

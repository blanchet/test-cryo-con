# qmake file for test-cryo-con-nok
include(../../conf/starting.pri)


TEMPLATE = app
TARGET = test-cryo-con-nok


QT = core

QMAKE_CFLAGS   *= $$system( pkg-config --cflags libmodbus )
QMAKE_CXXFLAGS *= $$system( pkg-config --cflags libmodbus )
LIBS           *= $$system( pkg-config --libs   libmodbus )


#HEADERS *= $$system( ls *.hpp )

SOURCES *= $$system( ls *.cpp )

include(../../conf/ending.pri)
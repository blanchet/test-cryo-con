
#include <stdio.h>
#include <cerrno>
#include <modbus.h>
#include <unistd.h>
#include <cstdlib>

/** @brief return the number of items in an array
 *
 * code from Google's Chromium project.
 * It safely fails if x is a C++ type that overloads []
 */
#define COUNT_OF(x) ((int)((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x]))))))

// ModbusTCP port
static const int CryoconModbusPort = 502;


/** @brief list of cryo channels */
float channels[8];

int main( int argc, const char** argv )
{
    // string with the IP address of Cryo-Con
    const char* cryoconIpAddress = 0;

    printf(
        "Program to test Cryo-con 18i with modbus.\n"
        "\n"
        "It recreates a new ModbusTcp Connection for each reading.\n"
        "    - Open TCP connection\n"
        "    - Read values\n"
        "    - Close TCP connection\n"
        "\n"
        "\n"
        "After a while, the cryo-con crashes, and it must be electrically reset.\n"
    );
    if (argc == 2) {
        cryoconIpAddress = argv[1];
    } else {
        printf(
            "\n"
            "Syntax:\n"
            "\n"
            "    %s IP_ADDRESS\n"
            "\n"
            "Example:\n"
            "    %s 10.251.251.10\n"
            "\n",
            argv[0],
            argv[0]
        );
        std::exit( 1 );
    }
    printf( "\n" );
    printf( "Connect to modbus %s:%d\n", cryoconIpAddress, CryoconModbusPort );
    printf( "The program will run for ever. Press CTRL+C to stop it\n" );

    modbus_t* ctx = modbus_new_tcp( cryoconIpAddress, CryoconModbusPort );



    int64_t counter = 0;
    while( true ) {
        uint16_t buf[64];
        const int nb = 16;

        // open a new modbus connection each time
        if (modbus_connect( ctx ) == -1) {
            fprintf( stderr,
                    "Connection to Modbus %s:%d has failed: %s\n",
                    cryoconIpAddress, CryoconModbusPort, modbus_strerror( errno ) );
        }

        int ret = modbus_read_registers( ctx, 0, nb, buf );
        if (ret == -1) {
            fprintf(stderr, "modbus_read_registers"
                    "( ctx=%p, a_startingAddr=0x%x, nb=0x%x, buf=%p) : %s\n",
                    ctx, 0, nb, buf,
                    modbus_strerror(errno));
        } else {
             for (int idx = 0; idx < COUNT_OF(channels); idx++) {
                // decode uint16_t into float
                union {
                    uint16_t c[2];
                    float temperature;
                } raw;
                raw.c[0] = buf[2 * idx];
                raw.c[1] = buf[2 * idx + 1];
                channels[idx] = raw.temperature;
            }
            printf( "%ld:  ", counter );
            for (int i = 0 ; i < COUNT_OF(channels) ; i++ ) {
                printf( "%f  ", channels[i] );
            }
            printf( "\n" );
        }
        //sleep( 1 );
        // close modbus connection
        modbus_close(ctx);
        counter++;
    }

    // free the modbus context
    modbus_free(ctx);
    ctx = 0;
    return 0;
}
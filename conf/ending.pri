# ###########################################################################
# Copyright (C) 2021 Institut de RadioAstronomie Millimetrique
#
# ABSTRACT: end file for test-cryo-con qmake projects
#
# AUTHOR: Sebastien BLANCHET
#
# CREATION DATE: Aug 11, 2021
# ###########################################################################

# This file should be included at the end of a project file

# create folders for generated files


CONFIG(debug, debug|release) {
    # Debug
     TARGET = $$join(TARGET,,,.debug)
    GENERATED_DIR = $${GENERATED_ROOT}/debug
} else {
    # Release
    GENERATED_DIR = $${GENERATED_ROOT}/release
    DEFINES += QT_NO_DEBUG_OUTPUT
}


OBJECTS_DIR = $${GENERATED_DIR}
MOC_DIR     = $${GENERATED_DIR}
RCC_DIR     = $${GENERATED_DIR}
UI_DIR      = $${GENERATED_DIR}

LIBS *= -L$${DESTDIR}

# add the .Debug suffix to project libs
CONFIG(debug, debug|release) {
    for( lib, PROJECT_LIBS ) {
        a = $$find(lib, -l.*)
        !isEmpty(a) {
            lib = $$join( lib, , "", ".debug")
        }
        LIBS *= $$lib
    }
} else {
    # release mode
    LIBS *= $$PROJECT_LIBS
}


# Set up include PATH
INCLUDEPATH *= $${ADDITIONAL_PATH}
DEPENDPATH  *= $${INCLUDEPATH}

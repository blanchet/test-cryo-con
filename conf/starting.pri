# ###########################################################################
# Copyright (C) 2021 Institut de RadioAstronomie Millimetrique
#
# ABSTRACT: Start file for test-cryo-con qmake projects
#
# AUTHOR: Sebastien BLANCHET
#
# CREATION DATE: Aug 11, 2021
# ###########################################################################

# This file should be included at the beginning of a project file

ROOT_DIR=$$system( dirname `pwd` )

LANGUAGE = C++
CONFIG *= warn_on qt

QMAKE_CXXFLAGS *= -fpermissive

GENERATED_ROOT=$${ROOT_DIR}/.generated


# Type 'qmake -r DESTDIR=/path/to'
# to redefine the default destination directory
isEmpty( DESTDIR ) {
    DESTDIR = $${ROOT_DIR}/bin
}


# To define an 'install' target:
# type 'qmake -r INSTALL_DIR=/path/to'
# and then 'make install'
!isEmpty( INSTALL_DIR ) {
    target.path += $${INSTALL_DIR}
    INSTALLS += target
}
